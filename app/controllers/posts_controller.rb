class PostsController < ApplicationController

	before_action :find_post, only: [:edit, :show, :update, :destroy]
	before_filter :authenticate_user!, except: [:index, :show]


	def index
		@posts = Post.all.order("created_at DESC")
	end

	def new
		@post = current_user.posts.build
	end

	def edit
	end

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			redirect_to @post, notice: "Successfully created new post"
		else
			render 'new'
		end
	end

	def show
	end

	def destroy
		@post.destroy
		redirect_to root_path, notice: "Successfully deleted post"
	end

	def update
		if @post.update(post_params)
			redirect_to @post
		else
			render 'edit'
		end
	end

	def upvote
		@post = Post.find(params[:id])
		@post.upvote_by current_user
		redirect_to :back
	end

	def downvote
		@post = Post.find(params[:id])
		@post.downvote_by current_user
		redirect_to :back
	end


	private
	def find_post
		@post = Post.find(params[:id])
	end

	def post_params
		params.require(:post).permit(:title, :desc)
	end

end
